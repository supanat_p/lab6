/**
 * This source code is Copyright 2015 by Supanat Pokturng
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A word counter that can count the frequency of each word.
 * sort by alphabet 
 * @author Supanat Pokturng
 * @version 2015.02.25
 */
public class WordCounter {
	
	/** 
	 * Declare a Map that keep a word to key
	 * and a frequency to a value. 
	 */
	private Map<String,Integer> map = new HashMap<String,Integer>();
	
	/**
	 * Add a word to a key of the map.
	 * if there is not have that word already,
	 * it will make a new key
	 * @param word is a word to keep in key in a map
	 */
	public void addWord( String word ){
		word = word.toLowerCase();
		if(map.containsKey(word)) {
			map.replace(word, map.get(word) + 1);
		}
		else
			map.put(word, 1);
	}
	
	/**
	 * Get a set of the word that is a key in the map.
	 * @return a set of the key
	 */
	public Set<String> getWords() {
		Set<String> words = map.keySet();
		return words;
	}
	
	/**
	 * Get a frequency of the word.
	 * @param word is a key to get a value that is frequency
	 * @return a frequency of that word
	 */
	public int getCount( String word ) {
		if(map.containsKey(word.toLowerCase()))
			return map.get(word.toLowerCase());
		return 0;
	}
	
	/**
	 * Get a sorted array's of the words.
	 * @return array of the words that sorted in array
	 */
	public String[] getSortedWords() {
		Set<String> set = getWords();
		String[] arrWord = new String[set.size()];
		set.toArray(arrWord);
		List<String> listWord = new ArrayList<String>();
		for(int i=0 ; i<arrWord.length ; i++) {
			listWord.add(arrWord[i]);
		}
		Collections.sort(listWord);
		String[] sortedList = new String[listWord.size()];
		listWord.toArray(sortedList);
		return sortedList;
	}
}
