/**
 * This source code is Copyright 2015 by Supanat Pokturng
 */
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * Word counter that can count number of each word
 * by use a WordCounter class.
 * @author Supanat Pokturng
 * @version 2015.02.25
 */
public class WordCount {
	
	/**
	 * Run the code by using WordCounter's class.
	 * @param args is an arguments
	 * @throws IOException 
	 */
	public static void main( String [] args ) throws IOException {
		
		/** Declare a WordCounter object */
		WordCounter counter = new WordCounter();
		
		String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt";
		URL url = new URL( FILE_URL );
		InputStream input = url.openStream();
		Scanner scanner = new Scanner( input );
		
		final String DELIMS = "[\\s,.\\?!\"():;]+";
		scanner.useDelimiter( DELIMS );
		
		while(scanner.hasNext()) {
			counter.addWord(scanner.next());
		}
		
		for(int i=0 ; i<20 ; i++) {
			System.out.println(counter.getSortedWords()[i] + " : " + counter.getCount(counter.getSortedWords()[i]));
		}
	}
}
